/**
 * 
 */
package com.kart.service.Impl;

import java.util.List;
  
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kart.dao.VendorRepository;
import com.kart.entity.Vendor;
import com.kart.service.VendorService;


/**
 * @author LENOVO
 *
 */

@Service
public class VendorServiceImpl implements VendorService {
	
	@Autowired
	private VendorRepository productRepository;

	public List<Vendor> getAllVendors() {
		return productRepository.findAll();
	}

	public List<Vendor> findAllByIds(Iterable<Long> ids) {
		return productRepository.findAllById(ids);
	}
	
	public List<Vendor> findByVendorName(String vendorName) {
		return productRepository.findByVendorName(vendorName);
	}
	
	public Vendor getOne(Long id) {
		return productRepository.getOne(id);
	}
	
	public Vendor saveAndFlush(Vendor vendor) {
		return productRepository.saveAndFlush(vendor);
	}

	public void flush() {
		productRepository.flush();
	}
	
	public List<Vendor> saveAll(Iterable<Vendor> vendors) {
		return productRepository.saveAll(vendors);
	}

	public void deleteInBatch(Iterable<Vendor> vendors) {
		productRepository.deleteInBatch(vendors);
	}

	public void deleteAllInBatch() {
		productRepository.deleteAllInBatch();
	}
	
}
