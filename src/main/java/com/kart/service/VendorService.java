package com.kart.service;

import java.util.List;

import com.kart.entity.Vendor;


public interface VendorService {
	
	List<Vendor> getAllVendors();

	List<Vendor> findAllByIds(Iterable<Long> ids);

	Vendor saveAndFlush(Vendor vendor);
	
	List<Vendor> saveAll(Iterable<Vendor> vendors);
	
	void flush();
	
	Vendor getOne(Long id);
	
	List<Vendor> findByVendorName(String vendorName);
	
	void deleteInBatch(Iterable<Vendor> vendors);

	void deleteAllInBatch();
	
}
