package com.kart.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kart.entity.Vendor;

@Repository
public interface VendorRepository extends JpaRepository<Vendor, Long>, CrudRepository<Vendor, Long> {
	
	List<Vendor> findByVendorName(String vendorName);
	
}
